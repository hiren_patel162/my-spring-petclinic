FROM openjdk:8
ADD target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar
ENTRYPOINT ["java", "-jar","spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar"]
EXPOSE 8080
